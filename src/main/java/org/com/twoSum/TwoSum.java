package org.com.twoSum;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {
  public int[] twoSum(int[] nums, int target) {
    // Create a hash map to store the number and its index
    Map<Integer, Integer> numToIndex = new HashMap<>();

    // Iterate through the array of numbers
    for (int i = 0; i < nums.length; i++) {
      int num = nums[i];
      // Calculate the complement
      int complement = target - num;

      // Check if the complement exists in the hash map
      if (numToIndex.containsKey(complement)) {
        // If found, return the indices of the two numbers
        return new int[] { numToIndex.get(complement), i };
      }

      // Add the current number and its index to the hash map
      numToIndex.put(num, i);
    }

    // In case there is no solution, though the problem states there will be exactly one solution
    throw new IllegalArgumentException("No two sum solution");
  }

  public static void main(String[] args) {
    TwoSum solution = new TwoSum();
    int[] nums = {2, 7, 11, 15};
    int target = 9;
    int[] result = solution.twoSum(nums, target);
    System.out.println("Indices: " + result[0] + ", " + result[1]);
  }
}

